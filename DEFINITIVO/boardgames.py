from flask import Flask,render_template,request,request as req
import interenciapkl as i
import entrenar as en
import conexion


app = Flask(__name__)


@app.route('/')
def index():
    return render_template ('home.html')

@app.route('/juegos')
def Juegos2():

    query=('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 50;')
    data=conexion.select(query)

    return render_template('infogamesNEW.html', contacts=data)


@app.route('/juegos2')
def Juegos():

    query=('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 20;')
    data=conexion.select(query)

    return render_template('infogamesOLD.html', contacts=data)


@app.route("/buscando", methods=["GET"])
def livesearch():
    juego= request.args.get("juego")
    juegos=i.buscar(juego)


    return juegos




@app.route('/recomanam')
def recomendarte():
    return render_template ('recomendarjuegos.html')




@app.route('/recomendando/', methods=['GET'])
def recomendar():
    juegos=req.args.get('juegos')
    ratings=req.args.get('ratings')

    cMatrix=i.recuperarDF()

    recomendacion20=i.usuario2(cMatrix,juegos,ratings)

    return recomendacion20



@app.route('/entrenar')
def entrenamiento2():

    corrMatrix=en.entrenar()
    pickle(corrMatrix)

    return "El entrenamiento está realizado"

    
    
    

