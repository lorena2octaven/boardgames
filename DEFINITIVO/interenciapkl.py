import pandas as pd

import conexion

import json





def recuperarDF():

    path="./corrmatrix2.pkl"
    cMatrix=pd.read_pickle(path)
    return cMatrix


def usuario2(corrMatrix,juegos,ratings):
    
    global j
    ratings=str(ratings)
    ratings=ratings.split(",")
    print(ratings)
    
    for rat in ratings:
        if rat!='':
            rat=float(rat)
            print(rat)

    juegos=str(juegos)
    juegos=juegos.split(",")

    for juego in juegos:
        if juego!="":
            juego=str(juego)

    df= pd.DataFrame(columns=juegos,data=[ratings])


    myusernow=df.loc[0]

    
    simCandidates = pd.Series()


    listajuego=[]
    for i in range(0, len(myusernow.index)):
        
        juego=str(myusernow.index[i])
        juego=juego.replace(",","")
        juego=juego.replace("(","")
        juego=juego.replace(")","")
        juego=juego.replace("'","")
        listajuego.append(juego)

        rat=myusernow.values[i]
        rat=float(rat)
        #print ("Añadiendo juegos similares a " + juego + "...")
    
    #Coge la correlacion de toda la fila del juego index I 
        sims = corrMatrix[str(juego)].dropna()
        #print(sims)
    
    #Cada valor de la correlacion de sims por la evaluacion del usuario
        sims = sims.map(lambda x: x * rat)
        #print("segundo sims"+str(sims))
    # Añadir el puntaje a la lista de candidatos similares
        simCandidates = simCandidates.append(sims)

    

    simCandidates.sort_values(inplace = True, ascending = False)

    




    simCandidates = simCandidates.groupby(simCandidates.index).sum()
    #print(simCandidates.head(10))
    filteredSims = simCandidates.drop(listajuego,errors='ignore')
    filteredSims.sort_values(inplace = True, ascending = False)

    recomendacion=filteredSims[0:20]


    
    
    #recomendacion=filteredSims[0:20].to_json()
 
    #print(recomendacion)
    
    lista=[]



    for x in range(0,len(recomendacion)):


    #for recom in recomendacion:

        

        query=('SELECT gamename FROM infogames where gamename="%s";'%(recomendacion.index[x]))
        name=conexion.selectone(query)
        print(name)
        print(query)

        query=("SELECT thumbnail FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        foto=conexion.selectone(query)

        query=("SELECT descript FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        desc=conexion.selectone(query)
        query=("SELECT minplayers FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        minp=conexion.selectone(query)
        query=("SELECT maxplayers FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        maxp=conexion.selectone(query)
        query=("SELECT playingtime FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        playtime=conexion.selectone(query)
        query=("SELECT minage FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        minage=conexion.selectone(query)
        query=("SELECT boardgamecategory FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        categorias=conexion.selectone(query)
        diccionario={'name':name,'foto':foto,'desc':desc,'minp':minp,'maxp':maxp,'playtime':playtime,'minage':minage,'categorias':categorias}
        lista.append(diccionario)
        print(lista)
        
        


    
    jsn=json.dumps(lista)
    #print(jsn)
        
    
        
    return jsn


"""
cm=recuperarDF()
lista='Catan,7 Wonders'
lista2='10,10'
u=usuario2(cm,lista, lista2)
print(u)
"""


def buscar(juego):

    juego="%"+str(juego)+"%"

    query=("Select gamename from infogames where gamename like '%s';" %(juego))

    juegos=conexion.select(query)
    lista=[]

    for juego in juegos:
        # for recom in recomendacion:

        query = ("SELECT gamename FROM infogames where gamename='%s';" % (juego))
        name = conexion.selectone(query)

        query = ("SELECT thumbnail FROM infogames where gamename='%s';" % (juego))
        foto = conexion.selectone(query)

        query = ("SELECT descript FROM infogames where gamename='%s';" % (juego))
        desc = conexion.selectone(query)
        query = ("SELECT minplayers FROM infogames where gamename='%s';" % (juego))
        minp = conexion.selectone(query)
        query = ("SELECT maxplayers FROM infogames where gamename='%s';" % (juego))
        maxp = conexion.selectone(query)
        query = ("SELECT playingtime FROM infogames where gamename='%s';" % (juego))
        playtime = conexion.selectone(query)
        query = ("SELECT minage FROM infogames where gamename='%s';" % (juego))
        minage = conexion.selectone(query)
        query = ("SELECT boardgamecategory FROM infogames where gamename='%s';" % (juego))
        categorias = conexion.selectone(query)
        diccionario = {'name': name, 'foto': foto, 'desc': desc, 'minp': minp, 'maxp': maxp, 'playtime': playtime,
                       'minage': minage, 'categorias': categorias}
        lista.append(diccionario)


    jsn = json.dumps(lista)


    return jsn


