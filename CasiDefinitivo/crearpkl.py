import pandas as pd
import numpy as np

def crear_pkl():

    ratings=pd.read_csv('.//bgg-15m-reviews.csv')
    games=pd.read_csv('.//2020-08-19.csv')
    ratings= ratings[['user','ID','rating']]
    games= games[['ID','Name']]
    ratings=ratings[:1000000]

    ratings = pd.merge(games, ratings)
    #ratings=ratings[:100000000]

    userRatings = ratings.pivot_table(index=['user'],columns=['Name'],values='rating')

    corrMatrix = userRatings.corr()

    corrMatrix = userRatings.corr(method='pearson', min_periods=100)

    return corrMatrix.head(10)




corrMatrix = crear_pkl()
print(corrMatrix)



"""
    games.to_pickle('./games.pkl')

    ratings.to_pickle('./ratings.pkl')
    


crear_pkl()

#games.to_pickle('./games.pkl')
ratings=pd.read_csv('.//bgg-15m-reviews.csv')
ratings.to_pickle('./ratings.pkl')
#user,rating,ID,name
"""
