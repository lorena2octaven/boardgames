from flask import Flask,render_template,request,url_for, jsonify,request as req
#from flask_mysqldb import MySQL
from flaskext.mysql import MySQL
import interenciapkl as i
import entrenar as en
import conexion


app = Flask(__name__)


# Mysql Connection
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '12345'
app.config['MYSQL_DB'] = 'boardgames'
#mysql = MySQL(app)
mysql = MySQL()
mysql.init_app(app)


# settings
app.secret_key = 'my secret_key'


@app.route('/')
def index():
    return render_template ('home.html')

@app.route('/juegos')
def Juegos2():
    #cur = mysql.get_db().cursor()
    #cur = mysql.connection.cursor()
    query=('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 200;')
    data=conexion.select(query)
    #cur.execute('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 20;')
    #data = cur.fetchall()

    return render_template('infogamesNEW.html', contacts=data)


@app.route('/juegos2')
def Juegos():
    #cur = mysql.get_db().cursor()
    #cur = mysql.connection.cursor()
    query=('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 20;')
    data=conexion.select(query)
    #cur.execute('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr asc limit 20;')
    #data = cur.fetchall()

    return render_template('infogamesOLD.html', contacts=data)


@app.route("/buscando", methods=["GET"])
def livesearch():
    searchbox= request.form.get("text")
    #cur = mysql.get_db().cursor()
    #cursor=mysql.connection.cursor()
    query = ("SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames where gamename='%s';" %(searchbox))
    result = conexion.selectone(query)
    #query="SELECT gamename from infogames where gamename LIKE '{}%' order by gamename".format(searchbox)
    #cursor.execute(query)
    #result=cursor.fetchall()
    return jsonify(result)




@app.route('/recomanam')
def recomendarte():
    return render_template ('recomendarjuegos.html')




@app.route('/recomendando/', methods=['GET'])
def recomendar():
    juegos=req.args.get('juegos')
    ratings=req.args.get('ratings')
    #corrMatrix=en.entrenando(ratings)
    #pickle(corrMatrix)

    cMatrix=i.recuperarDF()

    recomendacion20=i.usuario2(cMatrix,juegos,ratings)

    return recomendacion20



@app.route('/entrenar')
def entrenamiento2():

    corrMatrix=en.entrenar()
    pickle(corrMatrix)

    return "El entrenamiento está realizado"

    
    
    

    
 
