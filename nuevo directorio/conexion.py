import mysql.connector
from sqlalchemy import create_engine


def cnx():

    
    user = 'root'
    password = '12345'
    host = 'localhost'
    port = '3306'
    database = 'proyecto'
    engine = create_engine("mysql+pymysql://{0}:{1}@{2}/{3}?charset=utf8".format(user, password, host, database))

    return engine


def Cursor():

    conexion = mysql.connector.connect(host="localhost", user="root", passwd="12345", database="proyecto")
    cursor = conexion.cursor()

    return cursor, conexion


def Conexion(query):

    cursor=Cursor()

    
    cursor.execute(query)
    conexion.commit()
    cursor.close()
    conexion.close()

    

def select(query):

    cursor, conexion=Cursor()

    cursor.execute(query)
    filas= cursor.fetchall()
    cursor.close()
    conexion.close()

    return filas
    
def selectone(query):
    
    cursor, conexion=Cursor()

    cursor.execute(query)
    filas= cursor.fetchone()
    cursor.close()
    conexion.close()

    return filas


#Para los printers:
    #for fila in filas:   
        #print("Camino: " + str(fila[0]))
        #print(fila[1]+ "\n")
    


    #conexion.close()  
