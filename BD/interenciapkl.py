import pandas as pd
import numpy as np
import conexion
import operator
import json





def recuperarDF():

    path=("./corrmatrix2.pkl")
    cMatrix=pd.read_pickle(path)

    return cMatrix



def usuario(corrMatrix,juego1,rat1,juego2=None,rat2=None):
    
    df= pd.DataFrame(index=['user_id'])
    
    

    if juego2==None and rat2==None:

        
        df=df.append({juego1: rat1}, ignore_index=True)

    else:
        df=df.append({juego1: rat1, juego2: rat2}, ignore_index=True)
    
    
    
    myusernow=df.loc[1]
    #print(myusernow)
    
    simCandidates = pd.Series()
    
    for i in range(0, len(myusernow.index)):
        #print ("Añadiendo pelis similares a " + myusernow.index[i] + "...")
    
    #Coge la correlacion de toda la fila del juego index I 
        sims = corrMatrix[myusernow.index[i]].dropna()
        #print(sims)
    
    #Cada valor de la correlacion de sims por la evaluacion del usuario
        sims = sims.map(lambda x: x * myusernow.values[i])
        #print("segundo sims"+str(sims))
    # Añadir el puntaje a la lista de candidatos similares
        simCandidates = simCandidates.append(sims)
        #print("\n")
    

    simCandidates.sort_values(inplace = True, ascending = False)
    #print (simCandidates.head(10))
    




    simCandidates = simCandidates.groupby(simCandidates.index).sum()
    #print(simCandidates.head(10))
    filteredSims = simCandidates.drop(myusernow.index,errors='ignore')
    filteredSims.sort_values(inplace = True, ascending = False)
    #print(filteredSims)

    
    
    recomendacion=filteredSims[0:20].to_json()
    
    #print(recomendacion)
    
    #recomendacion={v: k for k, v in sorted(recomendacion.items(), key=lambda item: item[1])}
    #recomendacion=dict(sorted(recomendacion.items(),key=operator.itemgetter(1)))
    
    
    return recomendacion


"""
df=recuperarDF()
print(df)
U=usuario(df,'Pandemic',6,'Catan',10)
print(U)

"""



def usuario2(corrMatrix,juegos,ratings):
    
    ratings=str(ratings)
    ratings=ratings.split(",")
    print(ratings)
    
    for rat in ratings:
        if rat!='':
            rat=float(rat)
            print(rat)
    """
    listaratings=[]
    listaratings.append(ratings)
    listajuegos=[]"""
    juegos=str(juegos)
    juegos=juegos.split(",")

    for juego in juegos:
        if juego!="":
            juego=str(juego)
    #print(juegos)
    #listajuegos.append(juegos)
    
    
    #lista=[]
    #lista.append(ratings)
    df= pd.DataFrame(columns=[juegos],data=[ratings])
    """
    for rat in range(0,1,2):
        #for juego in juegos:
        ratings[rat+1]=float(ratings[rat+1])
        df=df.append({ratings[rat]:ratings[rat+1]}, ignore_index=True)
            #df=df.append({juego[0]:rat[0]}, ignore_index=True)

    print(df)
    
    for rat in range(2,len(ratings),2):
        #for juego in juegos:
        ratings[rat+1]=float(ratings[rat+1])
        df.iloc[1]=df.append({ratings[rat]:ratings[rat+1]}, ignore_index=True)
            #df=df.append({juego[0]:rat[0]}, ignore_index=True)
    """
    print(df)
    myusernow=df.loc[0]
    print("\nTodos los juegos introducidos")
    print(myusernow)
    
    simCandidates = pd.Series()


    listajuego=[]
    for i in range(0, len(myusernow.index)):
        
        juego=str(myusernow.index[i])
        juego=juego.replace(",","")
        juego=juego.replace("(","")
        juego=juego.replace(")","")
        juego=juego.replace("'","")
        listajuego.append(juego)

        rat=myusernow.values[i]
        rat=float(rat)
        print ("Añadiendo pelis similares a " + juego + "...")
    
    #Coge la correlacion de toda la fila del juego index I 
        sims = corrMatrix[str(juego)].dropna()
        #print(sims)
    
    #Cada valor de la correlacion de sims por la evaluacion del usuario
        sims = sims.map(lambda x: x * rat)
        #print("segundo sims"+str(sims))
    # Añadir el puntaje a la lista de candidatos similares
        simCandidates = simCandidates.append(sims)
        #print("\n")
    

    simCandidates.sort_values(inplace = True, ascending = False)
    #print (simCandidates.head(10))
    



    #print(listajuego)
    simCandidates = simCandidates.groupby(simCandidates.index).sum()
    #print(simCandidates.head(10))
    filteredSims = simCandidates.drop(listajuego,errors='ignore')
    filteredSims.sort_values(inplace = True, ascending = False)

    recomendacion=filteredSims[0:5]
    #print(filteredSims)

    
    
    #recomendacion=filteredSims[0:20].to_json()
 
    print(recomendacion)
    
    #recomendacion={v: k for k, v in sorted(recomendacion.items(), key=lambda item: item[1])}
    #recomendacion=dict(sorted(recomendacion.items(),key=operator.itemgetter(1)))
    
    lista=[]
    
    for x in range(0,len(recomendacion)):
    #for recom in recomendacion:
        
    
        query=("SELECT gamename FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        name=conexion.selectone(query)
        print(name)
        print(query)
        query=("SELECT thumbnail FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        foto=conexion.selectone(query)
        print(foto)
        query=("SELECT descript FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        desc=conexion.selectone(query)
        query=("SELECT minplayers FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        minp=conexion.selectone(query)
        query=("SELECT maxplayers FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        maxp=conexion.selectone(query)
        query=("SELECT playingtime FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        playtime=conexion.selectone(query)
        query=("SELECT minage FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        minage=conexion.selectone(query)
        query=("SELECT boardgamecategory FROM infogames where gamename='%s';"%(recomendacion.index[x]))
        categorias=conexion.selectone(query)
        diccionario={'name':name,'foto':foto,'desc':desc,'minp':minp,'maxp':maxp,'playtime':playtime,'minage':minage,'categorias':categorias}
        lista.append(diccionario)
        print(lista)
        
        


    
    jsn=json.dumps(lista)
    print(jsn)
        
    
        
    return jsn


"""
cm=recuperarDF()
lista='Catan,7 Wonders'
lista2='10,10'
u=usuario2(cm,lista, lista2)
print(u)
"""




