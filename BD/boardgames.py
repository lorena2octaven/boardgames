from flask import Flask,render_template,request, redirect, url_for, flash, jsonify,request as req
from flask_mysqldb import MySQL
import interenciapkl as i


app = Flask(__name__)


# Mysql Connection
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '12345'
app.config['MYSQL_DB'] = 'boardgames'
mysql = MySQL(app)

# settings
app.secret_key = 'my secret_key'


@app.route('/index')
def index():
    return render_template ('index.html')


@app.route('/juegos')
def Juegos():
    cur = mysql.connection.cursor()
    cur.execute('SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames order by bgr desc limit 20;')
    data = cur.fetchall()

    return render_template('infogames.html', contacts=data)


@app.route('/juegosrecomen', methods=['GET'])
def JuegosRecomen():
    juegos=req.args.get('juegos')
    cur = mysql.connection.cursor()
    recomendacion20=juegos
    query=("SELECT gamename,thumbnail,descript,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames where gamename='%s';"%(recomendacion20))
    cur.execute(query)
    data = cur.fetchall()

    return render_template('infogames.html', contacts=data)



@app.route('/recomanam')
def recomendar():
    return render_template ('RECOMENDARJUEGO4.html')




@app.route('/home')
def home():
    return render_template ('home.html')


@app.route('/recomendar/', methods=['GET'])
def recomendaciones():
    juego1=req.args.get('juego1')
    rat1= float(req.args.get('rat1'))
    
    #Si solo añades 1 peli hay que comentar esto:
    #peli2=req.args.get('peli2')
    #rat2=float(req.args.get('rat2'))
    #hasta aqui y quitarle los parametros a la funcion i.usuario(peli2,rat2)
    
    cMatrix=i.recuperarDF()
    recomendacion20=i.usuario(cMatrix,juego1,rat1)
    query=("SELECT gamename,thumbnail,description,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames where gamename='%s';"%(recomendacion20))
    data=JuegosRecomen(query)
    
    return render_template('infogames.html', contacts=data)













@app.route('/entrenando/', methods=['GET'])
def entrenamiento():
    juegos=req.args.get('juegos')
    ratings=req.args.get('ratings')
    #corrMatrix=en.entrenando(ratings)
    #pickle(corrMatrix)
    cMatrix=i.recuperarDF()
    recomendacion20=i.usuario2(cMatrix,juegos,ratings)
    """query=("SELECT gamename,thumbnail,description,minplayers,maxplayers,playingtime,minage,boardgamecategory FROM infogames where gamename='%s';"%(recomendacion20))
    data=JuegosRecomen(query)
    
    return render_template('infogames.html', contacts=data)"""
    
    return recomendacion20



@app.route('/entrenar')
def entrenamiento2():
    import entrenar as en
    corrMatrix=en.entrenar()
    pickle(corrMatrix)

    return "El entrenamiento está realizado"

    
    
    

    
 
